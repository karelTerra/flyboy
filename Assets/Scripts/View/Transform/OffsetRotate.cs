﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class OffsetRotate : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected float offset_Distance;
		[SerializeField]
		protected float offset_Time;
		[SerializeField]
		protected float speed;

		//// Protected
		// References
		protected Transform myTransform;
		// Primitives
		protected float startingHeight;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			myTransform = transform;
			startingHeight = myTransform.localPosition.y;
		}

		void Start ()
		{

		}
		
		void Update ()
		{
			Vector3 position = myTransform.localPosition;

			position.x = Mathf.Cos (offset_Time + (Time.timeSinceLevelLoad * speed)) * offset_Distance;
			position.y = startingHeight + Mathf.Sin (offset_Time + (Time.timeSinceLevelLoad * speed)) * offset_Distance;

			myTransform.localPosition = position;
		}

		/////////////////////////////////////////
		// Pulbic CircularRotate Functions

		/////////////////////////////////////////
		// Protected CircularRotate Functions

		/////////////////////////////////////////
		// Event Functions
	}
}
