﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class HorizontalDrift : MonoBehaviour
	{
		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected float driftVelocity;

		//// Protected
		// References
		protected new Transform transform;
		// Primitives
		protected bool shouldDrift = false;

		// Actions

		// Properties
		public bool IsDrifting { get { return shouldDrift; } }


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		private void Awake ()
		{
			transform = gameObject.transform;
		}

		void Start ()
		{

		}

		void Update ()
		{
			if (shouldDrift)
				transform.Translate(new Vector3(driftVelocity, 0f, 0f) * Time.deltaTime);
		}

		/////////////////////////////////////////
		// Public Application Functions

		public void SetVelocity(float driftVelocity)
		{
			this.driftVelocity = driftVelocity;

			if (Mathf.Abs(driftVelocity) > 0)
				shouldDrift = true;
			else if (driftVelocity == 0f)
				shouldDrift = false;
		}

		public void SetDriftEnabled (bool enabled)
		{
			shouldDrift = enabled;
		}

		/////////////////////////////////////////
		// Protected Application Functions

		/////////////////////////////////////////
		// Event Functions
	}
}
