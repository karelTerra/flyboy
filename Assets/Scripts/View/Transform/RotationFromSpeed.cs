﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class RotationFromSpeed : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		//[SerializeField]
		//protected bool XAxis;
		//[SerializeField]
		//protected bool YAxis;
		[SerializeField]
		protected bool ZAxis;

		[Header ("Rotation Limits")]
		[SerializeField]
		protected float pitchUpLimit;
		[SerializeField]
		protected float pitchDownLimit;

		[Space]
		[SerializeField]
		protected float speedAtMax;
		[SerializeField]
		protected float speedAtMin;

		//// Protected
		// References
		protected new Transform transform;
		// Primitives

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		private void Awake ()
		{
			transform = gameObject.transform;
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public Application Functions

		public void SetRotation (float speed)
		{
			if (ZAxis)
			{
				Vector3 rot = transform.localEulerAngles;

				if(speed > 0f)
				{
					rot.z = Mathf.Lerp (0, pitchUpLimit, MathUtility.NoimalizeInRange (0, speedAtMax, speed));
				}
				else if (speed < 0f)
				{
					rot.z = Mathf.Lerp (pitchDownLimit, 0, MathUtility.NoimalizeInRange (speedAtMin, 0, speed));
				}
				else
				{
					rot.z = 0f;
				}

				//rot.z = Mathf.Lerp (pitchDownLimit, pitchUpLimit, MathUtility.NoimalizeWithinRange (speedAtMin, speedAtMax, speed));


				transform.localEulerAngles = rot;
			}
		}

		/////////////////////////////////////////
		// Protected Application Functions

		/////////////////////////////////////////
		// Event Functions

	}
}
