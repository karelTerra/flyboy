﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	[RequireComponent (typeof (Collision2DReporter))]
	public class DisableOnCollision2D : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected bool parent;
		[SerializeField]
		protected bool destroy;

		//// Protected
		// References
		protected Collision2DReporter collision2DReporter;
		// Primitives

		// Actions

		// Properties


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			collision2DReporter = GetComponent<Collision2DReporter> ();
			collision2DReporter.CollisionReportAction += OnCollision;
		}


		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public DisableOnCollision2D Functions

		/////////////////////////////////////////
		// Protected DisableOnCollision2D Functions

		/////////////////////////////////////////
		// Event Functions

		protected void OnCollision (Collider2D collider)
		{
			if (parent)
			{
				if (destroy)
					Destroy (collider.transform.parent.gameObject);
				else
					collider.transform.parent.gameObject.SetActive (false);
			}
			else
			{
				if (destroy)
					Destroy (collider.gameObject);
				else
					collider.gameObject.SetActive (false);
			}
		}
	}
}
