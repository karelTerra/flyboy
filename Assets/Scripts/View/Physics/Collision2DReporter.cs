﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class Collision2DReporter : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected string collisionTag;

		//// Protected
		// References
		// Primitives

		// Actions
		public Action CollisionAction;
		public Action<Collider2D> CollisionReportAction;

		public Action<bool> CollisionDirAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{

		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		void OnCollisionEnter2D (Collision2D collision)
		{
			if (collision.collider.CompareTag (collisionTag))
			{
				if (CollisionAction != null)
					CollisionAction ();

				if (CollisionReportAction != null)
					CollisionReportAction (collision.collider);

				if (CollisionDirAction != null)
					CollisionDirAction (true);
			}
		}

		void OnTriggerEnter2D (Collider2D collider)
		{
			if (collider.CompareTag (collisionTag))
			{
				if (CollisionAction != null)
					CollisionAction ();
				
				if (CollisionReportAction != null)
					CollisionReportAction (collider);

				if (CollisionDirAction != null)
					CollisionDirAction (true);
			}
		}

		void OnCollisionExit2D (Collision2D collision)
		{
			if (collision.collider.CompareTag (collisionTag))
			{
				if (CollisionDirAction != null)
					CollisionDirAction (false);
			}
		}

		void OnTriggerExit2D (Collider2D collider)
		{
			if (collider.CompareTag (collisionTag))
			{
				if (CollisionDirAction != null)
					CollisionDirAction (false);
			}
		}

		/////////////////////////////////////////
		// Public Collision2DReporter Functions

		/////////////////////////////////////////
		// Protected Collision2DReporter Functions

		/////////////////////////////////////////
		// Event Functions
	}
}