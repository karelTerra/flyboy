﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	[RequireComponent (typeof (AudioSource))]
	public class AudioSFXSource : MonoBehaviour
	{

		// Readonly
		public static AudioSFXSource Instace;

		// Enum
		// Struct

		// Serialized

		//// Protected
		// References
		protected AudioSource audioSource;
		// Primitives

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			if (Instace != null)
				Destroy (gameObject);
			else
				Instace = this;

			audioSource = GetComponent<AudioSource> ();
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public AudioUISource Functions
		public static void RegisterSource (ref Action<AudioClip> sourcePlayAction)
		{
			sourcePlayAction += Instace.OnSourceAction;
		}

		/////////////////////////////////////////
		// Protected AudioUISource Functions

		/////////////////////////////////////////
		// Event Functions
		protected void OnSourceAction (AudioClip audioClip)
		{
			audioSource.PlayOneShot (audioClip);
		}
	}
}
