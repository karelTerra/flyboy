﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Terra.View
{
	public class AudioUI : MonoBehaviour , IPointerUpHandler
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected AudioClip audioClip;

		//// Protected
		// References
		// Primitives

		// Actions
		public Action<AudioClip> PointerDownAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake () 
		{
		}

		void Start () 
		{
			AudioSFXSource.RegisterSource (ref PointerDownAction);			
		}
		
		void Update () 
		{
			
		}

		/////////////////////////////////////////
		// Public AudioUI Functions

		/////////////////////////////////////////
		// Protected AudioUI Functions

		/////////////////////////////////////////
		// Event Functions

		public void OnPointerUp (PointerEventData eventData)
		{
			if (PointerDownAction != null)
				PointerDownAction (audioClip);
		}
	}
}
