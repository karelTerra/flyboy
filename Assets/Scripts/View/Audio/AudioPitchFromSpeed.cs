﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	[RequireComponent (typeof (AudioSource))]
	public class AudioPitchFromSpeed : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[Header ("Pitch Limits")]
		[SerializeField]
		protected float pitchUpLimit;
		[SerializeField]
		protected float pitchDownLimit;
		[Space]
		[SerializeField]
		protected float speedAtMax;
		[SerializeField]
		protected float speedAtMin;

		//// Protected
		// References
		protected AudioSource audioSource;
		// Primitives

		// Actions

		// Properties


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		private void Awake ()
		{
			audioSource = GetComponent<AudioSource> ();
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public AudioPitchFromSpeed Functions

		public void SetPitch (float speed)
		{
			audioSource.pitch = Mathf.Lerp (pitchDownLimit, pitchUpLimit, MathUtility.NoimalizeInRange (speedAtMin, speedAtMax, speed));
		}

		/////////////////////////////////////////
		// Private AudioPitchFromSpeed Functions

		/////////////////////////////////////////
		// Event Functions
	}
}
