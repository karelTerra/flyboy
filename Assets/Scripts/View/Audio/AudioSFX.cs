﻿using System;
using UnityEngine;

namespace Terra.View
{
	public class AudioSFX : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected AudioClip sfxAudioClip;

		[Header ("Reporter")]
		[SerializeField]
		protected Collision2DReporter collision2DReporter;

		//// Protected
		// References
		// Primitives

		// Actions
		public Action<AudioClip> SFXPlayClipAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			if (collision2DReporter != null)
				collision2DReporter.CollisionAction += PlaySFX;
		}

		void Start ()
		{
			AudioSFXSource.RegisterSource (ref SFXPlayClipAction);
		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public AudioSFX Functions

		public void PlaySFX ()
		{
			if (SFXPlayClipAction != null)
				SFXPlayClipAction (sfxAudioClip);
		}

		/////////////////////////////////////////
		// Protected AudioSFX Functions

		/////////////////////////////////////////
		// Event Functions
	}
}
