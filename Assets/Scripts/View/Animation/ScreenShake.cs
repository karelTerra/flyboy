﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class ScreenShake : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected bool shake = false;

		[SerializeField]
		protected float power = 1f;

		[SerializeField]
		protected float time = 0.1f;

		[SerializeField]
		protected AnimationCurve shakeTweenCurve;

		//// Protected
		// References
		protected new Transform transform;
		// Primitives
		protected Vector3 originalCameraPosition;
		protected bool isScreenShaking = false;
		protected Coroutine shakeRoutine;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			this.transform = gameObject.transform;
			originalCameraPosition = this.transform.localPosition;
		}

		void Start ()
		{

		}

		void Update ()
		{
			//if (shake && !isScreenShaking)
			//{
			//	shakeRoutine = StartCoroutine (ShakeRoutine ());
			//	isScreenShaking = true;
			//}
			//if (!shake && isScreenShaking)
			//{
			//	StopCoroutine (shakeRoutine);
			//	isScreenShaking = false;

			//	transform.localPosition = originalCameraPosition;
			//}
		}

		/////////////////////////////////////////
		// Public ScreenShake Functions

		public void StartShake (float length)
		{
			if (!isScreenShaking)
			{
				if (shakeRoutine != null)
					StopCoroutine (shakeRoutine);

				shakeRoutine = StartCoroutine (ShakeRoutine (length));
			}
		}

		/////////////////////////////////////////
		// Protected ScreenShake Functions

		protected IEnumerator ShakeRoutine (float time = -1)
		{
			isScreenShaking = true;
			float t = 0;
			bool first = true;

			while (t < time || time == -1)
			{
				Vector3 currentPosition = transform.localPosition;
				Vector3 newRandom = Random.insideUnitSphere;

				if (!first)
					while (!ValidNewPos (currentPosition, newRandom))
						newRandom = Random.insideUnitSphere;

				newRandom.z = 0f;
				Vector3 newPosition = originalCameraPosition + (newRandom * power);

				float T = 0f;

				while (T < this.time)
				{
					transform.localPosition = Vector3.Lerp (currentPosition, newPosition, shakeTweenCurve.Evaluate (T));

					t += Time.deltaTime;
					T += Time.deltaTime;
					yield return null;
				}
				first = false;
			}

			shakeRoutine = null;
			isScreenShaking = false;
		}

		protected bool ValidNewPos (Vector3 orig, Vector3 newPos)
		{
			Vector3 origDir = (orig - originalCameraPosition).normalized;

			if (Vector3.Dot (origDir, newPos) < 0)
				return true;

			return false;
		}


		/////////////////////////////////////////
		// Event Functions
	}
}
