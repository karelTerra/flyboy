﻿using System;
using System.Collections;
using UnityEngine;

namespace Terra.View
{
	[RequireComponent (typeof (CanvasGroup))]
	public class CanvasGroupAnimation : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected float animationTime;
		[SerializeField]
		protected AnimationCurve animationCurve;

		//// Protected
		// References
		protected CanvasGroup canvasGroup;
		protected Coroutine animationCoroutine;
		// Primitives
		protected float time;
		protected bool fadingIn;
		protected bool fading;

		// Actions
		public bool Fading
		{
			get
			{
				return fading;
			}
		}
		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			canvasGroup = GetComponent<CanvasGroup> ();
			time = 0f;
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public FullScreenFade Functions

		public void FadeIn (Action callback = null, float delay = 0f)
		{
			bool startFade = false;
			fading = true;

			if (animationCoroutine == null)
				startFade = true;
			else if (!fadingIn)
			{
				StopCoroutine (animationCoroutine);
				startFade = true;
			}

			if (startFade)
			{
				canvasGroup.blocksRaycasts = true;
				animationCoroutine = StartCoroutine (FadeInRoutine (callback, delay));
				fadingIn = true;
			}
		}

		public void FadeOut (bool fromOpaque = false, Action callback = null, float delay = 0f)
		{
			bool startFade = false;
			fading = true;

			if (animationCoroutine == null)
				startFade = true;
			else if (fadingIn)
			{
				StopCoroutine (animationCoroutine);
				startFade = true;
			}

			if (startFade)
			{
				if (fromOpaque)
				{
					canvasGroup.alpha = 1f;
					time = animationTime;
				}

				canvasGroup.interactable = false;
				canvasGroup.blocksRaycasts = false;
				animationCoroutine = StartCoroutine (FadeOutRoutine (callback, delay));
				fadingIn = false;
			}
		}

		/////////////////////////////////////////
		// Protected FullScreenFade Functions

		protected IEnumerator FadeInRoutine (Action callback = null, float delay = 0f)
		{ 
			float fadeTime = animationTime;

			if (delay > 0)
				yield return new WaitForSeconds (delay);

			// Make interactable when you start fading in
			canvasGroup.interactable = true;

			while (time < fadeTime)
			{
				float alpha = animationCurve.Evaluate (time / fadeTime);
				canvasGroup.alpha = alpha;

				time += Time.deltaTime;

				yield return null;
			}
			time = fadeTime;
			canvasGroup.alpha = 1f;

			if (callback != null)
				callback ();

			fadingIn = false;

			animationCoroutine = null;
			fading = false;
		}

		protected IEnumerator FadeOutRoutine (Action callback = null, float delay = 0f)
		{
			float fadeTime = animationTime;

			if (delay > 0)
				yield return new WaitForSeconds (delay);

			while (time > 0)
			{
				float alpha = animationCurve.Evaluate (time / fadeTime);
				canvasGroup.alpha = alpha;

				time -= Time.deltaTime;

				yield return null;
			}
			time = 0f;
			canvasGroup.alpha = 0f;

			if (callback != null)
				callback ();

			animationCoroutine = null;
			fading = false;
		}

		/////////////////////////////////////////
		// Event Functions

		/////////////////////////////////////////
		// Editor Functions

		[EditorButtonAttribute]
		private void DoFadeIn ()
		{
			FadeIn ();
		}

		[EditorButtonAttribute]
		private void DoFadeOut ()
		{
			FadeOut ();
		}

	}
}