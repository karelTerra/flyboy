﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	// This is not a great class. Animations should be more generic
	public class ScaleAnimation : MonoBehaviour 
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected Vector3 startScale;
		[SerializeField]
		protected Vector3 endScale;

		[SerializeField]
		protected float animationTime;
		[SerializeField]
		protected AnimationCurve animationCurve;

		//// Protected
		// References
		protected new Transform transform;
		protected Coroutine animationCoroutine;
		// Primitives
		protected float time;
		protected bool scaling;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake () 
		{
			this.transform = gameObject.transform;
			time = 0f;
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}

		/////////////////////////////////////////
		// Public ScaleAnimation Functions

		public void StartScaling (Action callback = null, float delay = 0f)
		{
			bool startScale = false;

			if (animationCoroutine == null)
				startScale = true;
			else if (!scaling)
			{
				StopCoroutine (animationCoroutine);
				startScale = true;
			}

			if (startScale)
			{
				animationCoroutine = StartCoroutine (ScaleInRoutine (callback, delay));
				scaling = true;
			}
		}

		/////////////////////////////////////////
		// Protected ScaleAnimation Functions

		protected void SetScale(Vector3 scale)
		{
			transform.localScale = scale;
		}

		protected IEnumerator ScaleInRoutine (Action callback = null, float delay = 0f)
		{
			float fadeTime = animationTime;

			if (delay > 0)
				yield return new WaitForSeconds (delay);

			float t = 0;
			time = 0f;
			while (time < fadeTime)
			{
				t = animationCurve.Evaluate (time / fadeTime);
				SetScale (Vector3.Lerp (startScale, endScale, t));

				time += Time.deltaTime;

				yield return null;
			}
			time = fadeTime;

			t = animationCurve.Evaluate (time / fadeTime);
			SetScale (Vector3.Lerp (startScale, endScale, t));

			if (callback != null)
				callback ();

			scaling = false;

			animationCoroutine = null;
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
