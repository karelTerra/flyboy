﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class GameScroll : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct
		[System.Serializable]
		public struct Parallax
		{
			public GameObject ParalaxGO;
			public float NearSpeed;
			public float MidSpeed;
			public float FarSpeed;

			public float ForeOffset { get; set; }
			public float MidOffset { get; set; }
			public float BackOffset { get; set; }

			public Material Material { get; private set; }
			public void Initialize ()
			{
				Material = ParalaxGO.GetComponent<Renderer> ().material;
				ResetScroll ();
			}
			public void ResetScroll ()
			{
				Material.SetFloat ("_ForeScroll", 0f);
				Material.SetFloat ("_MidScroll", 0f);
				Material.SetFloat ("_BackScroll", 0f);
			}
		}

		[System.Serializable]
		public struct ScrollingSprite
		{
			public SpriteRenderer sprite;
			public float minSpeed;
			public float maxSpeed;

			public float Scroll { get; set; }

			public Material Material { get; private set; }
			public void Initialize ()
			{
				Material = sprite.sharedMaterial;
				ResetScroll ();
			}
			public void ResetScroll ()
			{
				Material.SetFloat ("_Scroll", 0f);
			}
		}

		[System.Serializable]
		public struct Translation
		{
			public HorizontalDrift drift;
			public float maxSpeed;
		}

		[System.Serializable]
		public struct CloudParticleSystem
		{
			public ParticleSystem particleSystem;
			public float minSpeed;
			public float maxSpeed;
		}

		// Serialized
		[Header ("Parallax")]
		[SerializeField]
		protected Parallax parallax;

		[Header ("Scrolling")]
		[SerializeField]
		protected ScrollingSprite [] scrollingSprites;

		[Header ("Drift")]
		[SerializeField]
		protected Translation [] driftingObjects;

		[Header ("Cloud Particles")]
		[SerializeField]
		protected CloudParticleSystem [] cloudParticleSystems;

		[Header ("Animation")]
		[SerializeField]
		protected float animationTime;
		[SerializeField]
		protected AnimationCurve animationCurve;

		//// Protected
		// References
		protected Coroutine animationCoroutine;
		// Primitives
		protected float time;
		protected float t;

		protected bool scrolling;
		protected bool speedingUp;
		protected bool slowingDown;

		// Actions
		public Action<float> ScrollLerpTChangedAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			time = 0f;

			parallax.Initialize ();

			for (int i = 0; i < scrollingSprites.Length; i++)
			{
				scrollingSprites [i].Initialize ();
			}
		}

		void Start ()
		{

		}

		void Update ()
		{
			if (scrolling)
			{
				// Parlax
				SetParalaxScroll (t);

				// Scrolling Sprites
				SetScrollingSpriteScroll (t);
			}
		}

		/////////////////////////////////////////
		// Public ParalaxAnimation Functions

		public void PauseScroll()
		{
			scrolling = false;
			SetDriftSpeed (0f);
			SetCloudSimulationSpeed (0f);
			InvokeOnScrollSpeedChangedAction (0f);
		}

		public void ResumeScroll()
		{
			scrolling = true;
			SetDriftSpeed (1f);
			SetCloudSimulationSpeed (1f);
			InvokeOnScrollSpeedChangedAction (1f);
		}

		public void SpeedUp (Action callback = null, float delay = 0f)
		{
			bool startFade = false;

			scrolling = true;

			if (animationCoroutine == null)
				startFade = true;
			else if (!speedingUp)
			{
				StopCoroutine (animationCoroutine);
				startFade = true;
			}

			if (startFade)
			{
				animationCoroutine = StartCoroutine (SpeedUpRoutine (callback));
				speedingUp = true;
			}
		}

		public void SlowDown (Action callback = null, float delay = 0f)
		{
			bool startFade = false;

			if (animationCoroutine == null)
				startFade = true;
			else if (speedingUp)
			{
				StopCoroutine (animationCoroutine);
				startFade = true;
			}

			if (startFade)
			{
				animationCoroutine = StartCoroutine (SlowDownRoutine (() => { if(callback != null) callback (); scrolling = false; }));
				speedingUp = false;
			}
		}

		/////////////////////////////////////////
		// Protected ParalaxAnimation Functions

		protected IEnumerator SpeedUpRoutine (Action callback = null, float delay = 0f)
		{
			float fadeTime = animationTime;

			if (delay > 0)
				yield return new WaitForSeconds (delay);


			while (time < fadeTime)
			{
				t = animationCurve.Evaluate (time / fadeTime);
				InvokeOnScrollSpeedChangedAction (t);
				
				time += Time.deltaTime;

				SetDriftSpeed (t);
				SetCloudSimulationSpeed (t);

				yield return null;
			}
			time = fadeTime;
			t = animationCurve.Evaluate (time / fadeTime);
			InvokeOnScrollSpeedChangedAction (t);
			
			SetDriftSpeed (t);
			SetCloudSimulationSpeed (t);

			if (callback != null)
				callback ();

			speedingUp = false;

			animationCoroutine = null;
		}

		protected IEnumerator SlowDownRoutine (Action callback = null, float delay = 0f)
		{
			float fadeTime = animationTime;

			if (delay > 0)
				yield return new WaitForSeconds (delay);

			while (time > 0)
			{
				t = animationCurve.Evaluate (time / fadeTime);
				InvokeOnScrollSpeedChangedAction (t);

				SetDriftSpeed (t);
				SetCloudSimulationSpeed (t);

				time -= Time.deltaTime;

				yield return null;
			}
			time = 0f;
			t = animationCurve.Evaluate (time / fadeTime);
			InvokeOnScrollSpeedChangedAction (t);

			SetDriftSpeed (t);
			SetCloudSimulationSpeed (t);

			if (callback != null)
				callback ();

			animationCoroutine = null;
		}

		protected void SetParalaxScroll (float lerpValue)
		{
			if (parallax.Material != null)
			{
				parallax.ForeOffset += Mathf.Lerp (0f, parallax.NearSpeed, lerpValue) * Time.deltaTime;
				parallax.MidOffset += Mathf.Lerp (0f, parallax.MidSpeed, lerpValue) * Time.deltaTime;
				parallax.BackOffset += Mathf.Lerp (0f, parallax.FarSpeed, lerpValue) * Time.deltaTime;

				// Fix for juttery parralax on moblie, think its affected by large offsets
				if (parallax.ForeOffset >= 2f)
					parallax.ForeOffset -= 2f;
				if (parallax.MidOffset >= 2f)
					parallax.MidOffset -= 2f;
				if (parallax.BackOffset >= 2f)
					parallax.BackOffset -= 2f;

				parallax.Material.SetFloat ("_ForeScroll", parallax.ForeOffset);
				parallax.Material.SetFloat ("_MidScroll", parallax.MidOffset);
				parallax.Material.SetFloat ("_BackScroll", parallax.BackOffset);
			}
		}

		protected void SetScrollingSpriteScroll (float lerpValue)
		{
			for (int i = 0; i < scrollingSprites.Length; i++)
			{
				scrollingSprites [i].Scroll += Mathf.Lerp (0f, scrollingSprites [i].maxSpeed, lerpValue) * Time.deltaTime;

				if (scrollingSprites [i].Scroll >= 2f)
					scrollingSprites [i].Scroll -= 2f;

				scrollingSprites [i].Material.SetFloat ("_Scroll", scrollingSprites [i].Scroll);
			}
		}

		protected void SetDriftSpeed (float t)
		{
			for (int i = 0; i < driftingObjects.Length; i++)
			{
				float speed = Mathf.Lerp (0f, driftingObjects [i].maxSpeed, t);
				driftingObjects [i].drift.SetVelocity (speed);
			}
		}

		protected void SetCloudSimulationSpeed (float t)
		{
			for (int i = 0; i < cloudParticleSystems.Length; i++)
			{
				CloudParticleSystem cloudSystem = cloudParticleSystems [i];

				float simulationSpeed = Mathf.Lerp (cloudSystem.minSpeed, cloudSystem.maxSpeed, t);

				ParticleSystem.MainModule cloudMain = cloudSystem.particleSystem.main;
				cloudMain.simulationSpeed = simulationSpeed;
			}
		}

		protected void SetDefaultValues ()
		{

		}

		/////////////////////////////////////////
		// Event Functions

		protected void InvokeOnScrollSpeedChangedAction(float t)
		{
			if (ScrollLerpTChangedAction != null)
				ScrollLerpTChangedAction (t);
		}

		/////////////////////////////////////////
		// Editor Functions

		[EditorButtonAttribute]
		private void DoRampUp ()
		{
			SpeedUp ();
		}

		[EditorButtonAttribute]
		private void DoRampDown ()
		{
			SlowDown ();
		}
	}
}
