﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Terra.View
{
	public class AlphaAnimation : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected SpriteRenderer spriteRenderer;
		[SerializeField]
		protected MeshRenderer meshRenderer;

		[Space]
		[SerializeField]
		protected float startValue;
		[SerializeField]
		protected float endValue;

		[Space]
		[SerializeField]
		protected float animationTime;
		[SerializeField]
		protected AnimationCurve animationCurve;

		[Space]
		[SerializeField]
		protected bool SetEndValueOnStart;

		//// Protected
		// References
		protected Coroutine animationRoutine;
		// Primitives
		protected float time;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			time = 0f;
		}

		void Start ()
		{
			if (SetEndValueOnStart)
				SetAlpha (endValue);
		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public AlphaAnimation Functions

		public void Antimate (Action callback = null, float delay = 0f)
		{
			if (animationRoutine != null)
			{
				StopCoroutine (animationRoutine);
				animationRoutine = null;
			}

			animationRoutine = StartCoroutine (AnimationRoutine (callback, delay));
		}

		/////////////////////////////////////////
		// Protected AlphaAnimation Functions

		protected void SetAlpha (float alpha)
		{
			if (spriteRenderer != null)
			{
				Color spriteColor = spriteRenderer.color;
				spriteColor.a = alpha;
				spriteRenderer.color = spriteColor;
			}

			if (meshRenderer != null)
			{
				Color meshColor = meshRenderer.material.color;
				meshColor.a = alpha;
				meshRenderer.material.color = meshColor;
			}
		}


		protected IEnumerator AnimationRoutine (Action callback = null, float delay = 0f)
		{
			if (delay > 0f)
				yield return new WaitForSeconds (delay);

			float t = 0f;
			time = 0f;
			while (time < animationTime)
			{
				t = animationCurve.Evaluate (time / animationTime);
				SetAlpha (Mathf.Lerp (startValue, endValue, t));

				time += Time.deltaTime;

				yield return null;
			}
			time = animationTime;

			t = animationCurve.Evaluate (time / animationTime);
			SetAlpha (Mathf.Lerp (startValue, endValue, t));

			if (callback != null)
				callback ();

			animationRoutine = null;
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
