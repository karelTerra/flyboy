﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class PositionAnimation : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected Vector3 startPosition;
		[SerializeField]
		protected Vector3 endPosition;

		[Space]
		[SerializeField]
		protected float animationTime;
		[SerializeField]
		protected AnimationCurve animationCurve;

		[Space]
		[SerializeField]
		protected bool loop;
		[SerializeField]
		protected int numberOfLoops;
		[SerializeField]
		protected bool pingpong;

		//// Protected
		// References
		protected new Transform transform;
		protected Coroutine animationCoroutine;
		// Primitives
		protected float time;
		protected bool animating;
		protected bool wasAnimating;
		protected int loopCount;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			this.transform = gameObject.transform;
			time = 0f;
		}

		void Start ()
		{

		}

		void Update ()
		{
			if (wasAnimating)
			{
				if (loop && loopCount < numberOfLoops)
				{
					if (pingpong)
					{
						Vector3 temp = startPosition;
						startPosition = endPosition;
						endPosition = temp;
					}
					Animate ();
					loopCount++;
				}
				wasAnimating = false;
			}
		}

		/////////////////////////////////////////
		// Public ScaleAnimation Functions

		public void Animate (Action callback = null, float delay = 0f)
		{
			bool start = false;

			if (animationCoroutine == null)
				start = true;
			else if (!animating)
			{
				StopCoroutine (animationCoroutine);
				start = true;
			}

			if (start)
			{
				animationCoroutine = StartCoroutine (AnimationRoutine (callback, delay));
				animating = true;
			}
		}

		public void Reset ()
		{
			loopCount = 0;
		}

		/////////////////////////////////////////
		// Protected ScaleAnimation Functions

		protected void SetPosition (Vector3 scale)
		{
			transform.localPosition = scale;
		}

		protected IEnumerator AnimationRoutine (Action callback = null, float delay = 0f)
		{
			if (delay > 0)
				yield return new WaitForSeconds (delay);

			float t = 0f;
			time = 0f;
			while (time < animationTime)
			{
				t = animationCurve.Evaluate (time / animationTime);
				SetPosition (Vector3.Lerp (startPosition, endPosition, t));

				time += Time.deltaTime;

				yield return null;
			}
			time = animationTime;

			t = animationCurve.Evaluate (time / animationTime);
			SetPosition (Vector3.Lerp (startPosition, endPosition, t));

			if (callback != null)
				callback ();

			animating = false;

			animationCoroutine = null;
			wasAnimating = true;
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
