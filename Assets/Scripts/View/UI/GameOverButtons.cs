﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terra.View
{
	public class GameOverButtons : MonoBehaviour 
	{

		// Readonly

		// Enum
		// Struct

		// Serialized

		[SerializeField]
		protected Button BackToMenuButton;

		[SerializeField]
		protected Button RestartButton;

		//// Protected
		// References
		// Primitives

		// Actions
		public Action BackToMenuSelectedAction;
		public Action RestartSelectedAction;
		
		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour
		
		void Awake () 
		{
			BackToMenuButton.onClick.AddListener (() => OnBackToMenuSelected ());
			RestartButton.onClick.AddListener (() => OnRestartSelected ());
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}
		
		/////////////////////////////////////////
		// Public GameOverButtons Functions
		
		/////////////////////////////////////////
		// Protected GameOverButtons Functions
		
		/////////////////////////////////////////
		// Event Functions
		protected void OnBackToMenuSelected ()
		{
			if (BackToMenuSelectedAction != null)
				BackToMenuSelectedAction ();
		}

		protected void OnRestartSelected()
		{
			if (RestartSelectedAction != null)
				RestartSelectedAction ();
		}
	}
}
