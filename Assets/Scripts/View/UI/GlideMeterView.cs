﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terra.View
{
	public class GlideMeterView : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[Header ("Meter")]
		[SerializeField]
		protected Image meterImage;

		[SerializeField]
		protected Gradient meterActiveGradient;

		[SerializeField]
		protected Color meterInactiveColor = Color.white;

		[SerializeField]
		protected ScaleAnimation iconScaleAnimation;

		[Header ("Icon")]
		[SerializeField]
		protected Image iconImage;

		[SerializeField]
		protected Sprite iconActiveSprite;

		[SerializeField]
		protected Sprite iconInactiveSprite;

		//// Protected
		// References
		// Primitives
		protected bool viewStateActive = true;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			iconImage.sprite = iconActiveSprite;
		}

		void Start ()
		{

		}

		void Update ()
		{
			if (viewStateActive)
				ApplyGradientColorToImageFromFill ();
		}

		/////////////////////////////////////////
		// Public GlideMeterView Functions
		public void SetViewStateActive (bool active)
		{
			viewStateActive = active;

			if (active)
			{
				iconImage.sprite = iconActiveSprite;
			}
			else
			{
				iconImage.sprite = iconInactiveSprite;
				meterImage.color = meterInactiveColor;
			}
		}

		public void SetFill (float fill)
		{
			float fillAmaount = Mathf.Clamp01 (fill);
			meterImage.fillAmount = fillAmaount;
		}

		public void PlayIconAnimation ()
		{
			iconScaleAnimation.StartScaling ();
		}

		/////////////////////////////////////////
		// Protected GlideMeterView Functions
		protected void ApplyGradientColorToImageFromFill ()
		{
			float fill = meterImage.fillAmount;
			Color gradientColor = meterActiveGradient.Evaluate (fill);
			meterImage.color = gradientColor;
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
