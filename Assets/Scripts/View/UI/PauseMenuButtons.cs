﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terra
{
	public class PauseMenuButtons : MonoBehaviour 
	{

		// Readonly

		// Enum
		// Struct

		// Serialized

		[SerializeField]
		protected Button BackToMenuButton;
		[SerializeField]
		protected Button RestartButton;
		[SerializeField]
		protected Button ResumeButton;

		//// Protected
		// References
		// Primitives

		// Actions
		public Action BackToMenuSelectedAction;
		public Action RestartSelectedAction;
		public Action ResumeSelectedAction;
		
		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour
		
		void Awake () 
		{
			BackToMenuButton.onClick.AddListener (OnBackToMenuSelected);
			RestartButton.onClick.AddListener (OnRestartSelected);
			ResumeButton.onClick.AddListener (OnResumeSelected);
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}
		
		/////////////////////////////////////////
		// Public PauseManuButtons Functions
		
		/////////////////////////////////////////
		// Protected PauseManuButtons Functions
		
		/////////////////////////////////////////
		// Event Functions
		protected void OnBackToMenuSelected()
		{
			if (BackToMenuSelectedAction != null)
				BackToMenuSelectedAction ();
		}

		protected void OnRestartSelected()
		{
			if (RestartSelectedAction != null)
				RestartSelectedAction ();
		}

		protected void OnResumeSelected()
		{
			if (ResumeSelectedAction != null)
				ResumeSelectedAction ();
		}
	}
}
