﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Terra.View
{
	public class GameUIManager : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[Header ("Pause Button")]
		[SerializeField]
		protected Button PauseButton;

		[Header ("Menu Buttons")]
		[SerializeField]
		protected PauseMenuButtons pauseMenuButtons;
		[SerializeField]
		protected GameOverButtons gameOverButtons;

		[Header ("Score")]
		[SerializeField]
		protected ScoreView scoreView;
		[SerializeField]
		protected TextMeshProUGUI scoreValueTMPText;
		[SerializeField]
		protected TextMeshProUGUI bestValueTMPText;
		[Space]
		[SerializeField]
		protected GameObject NewBestScore;

		[Header ("Animations")]
		[SerializeField]
		protected CanvasGroupAnimation gameUICanvasGroupAnimation;
		[SerializeField]
		protected CanvasGroupAnimation gameUIMenuCanvasGroupAnimation;
		[SerializeField]
		protected CanvasGroupAnimation glideMeterGroupAnimation;

		//// Protected
		// References
		protected Coroutine menuFadeRoutine;
		// Primitives
		protected bool isPaused;
		protected bool isGameOver;
		protected int bestScore;

		// Actions
		public Action BackToMenuAction;
		public Action PauseSelectedAction;
		public Action ResumeSelectedAction;
		public Action RestartSelectedAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			pauseMenuButtons.BackToMenuSelectedAction += OnBackToMenuSelected;
			pauseMenuButtons.RestartSelectedAction += OnRestartSelected;
			pauseMenuButtons.ResumeSelectedAction += OnResumeSelected;

			gameOverButtons.BackToMenuSelectedAction += OnBackToMenuSelected;
			gameOverButtons.RestartSelectedAction += OnRestartSelected;

			PauseButton.onClick.AddListener (OnPauseSelected);
		}

		void Start ()
		{
			DisableMenus ();
			SetNewBestEnabled (false);
		}

		void Update ()
		{

		}

		public void Reset ()
		{
			isPaused = false;
			scoreView.Reset ();
		}

		/////////////////////////////////////////
		// Public GameUIManager Functions

		public void ShowGameUI ()
		{
			gameUICanvasGroupAnimation.FadeIn ();
			glideMeterGroupAnimation.FadeIn ();
		}

		public void ShowGameOverMenu ()
		{
			//Fix for double buttons
			if (gameUIMenuCanvasGroupAnimation.Fading)
				DisableMenus ();

			isGameOver = true;
			UpdateMenuScore (true);
			gameOverButtons.gameObject.SetActive (true);
			HideGameUI ();
			gameUIMenuCanvasGroupAnimation.FadeIn ();
		}


		/////////////////////////////////////////
		// Protected GameUIManager Functions

		protected void HideGameUI ()
		{
			gameUICanvasGroupAnimation.FadeOut ();
			glideMeterGroupAnimation.FadeOut ();
		}

		protected void ShowPauseMenu ()
		{
			isPaused = true;
			UpdateMenuScore ();
			pauseMenuButtons.gameObject.SetActive (true);
			gameUIMenuCanvasGroupAnimation.FadeIn ();
			HideGameUI ();
		}

		protected void HideGameUIMenu (Action action = null)
		{
			gameUIMenuCanvasGroupAnimation.FadeOut (false, () => { if (action != null) action (); DisableMenus (); });
		}

		protected void DisableMenus ()
		{
			PauseButton.interactable = true;
			pauseMenuButtons.gameObject.SetActive (false);
			gameOverButtons.gameObject.SetActive (false);
			isPaused = false;
			isGameOver = false;
		}

		protected void SetNewBestEnabled (bool enabled)
		{
			NewBestScore.gameObject.SetActive (enabled);
		}

		protected void UpdateMenuScore (bool saveBest = false)
		{
			int score = scoreView.Score;
			int best = PlayerPrefs.GetInt ("Best", 0);

			scoreValueTMPText.text = score.ToString ();

			if (score > best)
			{
				if (saveBest)
				{
					SetNewBestEnabled (true);
					PlayerPrefs.SetInt ("Best", score);
					best = score;
				}
			}
			else
			{
				SetNewBestEnabled (false);
			}


			bestValueTMPText.text = best.ToString ();
		}

		protected void SetDefaultMenuState ()
		{
			DisableMenus ();
			SetNewBestEnabled (false);
		}

		/////////////////////////////////////////
		// Event Functions
		protected void OnBackToMenuSelected ()
		{
			if (BackToMenuAction != null)
				BackToMenuAction ();

			HideGameUI ();
		}

		protected void OnPauseSelected ()
		{
			if (PauseSelectedAction != null)
				PauseSelectedAction ();

			ShowPauseMenu ();
		}

		protected void OnResumeSelected ()
		{
			HideGameUIMenu (() =>
		   {
			   if (ResumeSelectedAction != null)
				   ResumeSelectedAction ();
		   });
			ShowGameUI ();
		}

		protected void OnRestartSelected ()
		{
			if (RestartSelectedAction != null)
				RestartSelectedAction ();

			if (isPaused || isGameOver)
				HideGameUIMenu ();

			HideGameUI ();
		}
	}
}
