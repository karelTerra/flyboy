﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Terra
{
	public class DevButton : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected Button button;

		[SerializeField]
		protected int triggerCount = 3;

		[SerializeField]
		protected DevButtonEvent triggerEvent;

		//// Protected
		// References
		// Primitives
		protected int count = 0;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			button.onClick.AddListener (OnButtonPressed);
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public DevButton Functions

		/////////////////////////////////////////
		// Protected DevButton Functions

		/////////////////////////////////////////
		// Event Functions
		protected void OnButtonPressed ()
		{
			count++;

			if(count > triggerCount)
			{
				count = 0;

				if (triggerEvent != null)
					triggerEvent.Invoke ();
			}
		}
	}

	[System.Serializable]
	public class DevButtonEvent : UnityEvent
	{

	}
}
