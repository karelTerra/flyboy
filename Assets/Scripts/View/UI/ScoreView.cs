﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Terra.Interface;

namespace Terra.View
{
	public class ScoreView : MonoBehaviour, IResetable
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected TextMeshProUGUI scoreValueText;
		[SerializeField]
		protected Collision2DReporter scoreCollisionReporter;
		[SerializeField]
		protected ScaleAnimation scaleAnimation;

		//// Protected
		// References
		// Primitives
		protected int score;

		// Actions / Func
		public Func<bool> GameOver;
		public Func<bool> TutorialComplete;

		// Properties
		public int Score { get { return score; } }


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		private void Awake ()
		{
			scoreCollisionReporter.CollisionAction += OnGoalCollision;
		}

		void Start ()
		{
			score = 0;
			SetScoreView ();
		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public Application Functions

		public void Reset ()
		{
			score = 0;
			SetScoreView ();
		}

		/////////////////////////////////////////
		// Protected Application Functions

		protected void SetScoreView ()
		{
			scoreValueText.text = score.ToString ();
		}

		protected void PlayAnimation ()
		{
			scaleAnimation.StartScaling ();
		}

		/////////////////////////////////////////
		// Event Functions

		protected void OnGoalCollision ()
		{
			if (!GameOver () && TutorialComplete())
			{
				score++;
				SetScoreView ();
				PlayAnimation ();
			}
		}
	}
}
