﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terra
{
	public class UIImageButtonToggleView : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected Button button;
		[SerializeField]
		protected Image image;

		[Space]
		[SerializeField]
		protected Sprite onSprite;
		[SerializeField]
		protected Sprite offSprite;

		[Space]
		[SerializeField]
		protected bool setOnStart;
		[SerializeField]
		protected bool defaultState;

		//// Protected
		// References
		// Primitives
		protected bool isToggled;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			button.onClick.AddListener (OnButtonSelected);
		}

		void Start ()
		{
			if(setOnStart)
				SetToggle (defaultState);
		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public ToggleButtonView Functions

		public void SetToggle (bool enabled)
		{
			image.sprite = enabled ? onSprite : offSprite;
			isToggled = enabled;
		}

		/////////////////////////////////////////
		// Protected ToggleButtonView Functions

		protected void OnButtonSelected ()
		{
			SetToggle (!isToggled);
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
