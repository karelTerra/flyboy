﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terra.View
{
	public class TouchInput : MonoBehaviour
	{

		// Readonly
		protected readonly float HOLD_THESHOLD = 0.2f;
		protected readonly string DEBUG_HOLD_TEXT = "HOLD TIME: ";

		// Enum
		// Struct

		// Serialized
		[Header ("Debug")]
		[SerializeField]
		protected Text debugText_HoldTime;

		//// Protected
		// References
		// Primitives
		protected float holdTime = 0f;
		protected bool touched = false;

		// Actions
		public Action TouchClickAction;
		public Action<bool> TouchHoldAction;

		// Properties


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{

		}

		void Start ()
		{

		}
		
		protected void Update ()
		{
			ProccessTouch ();
		}

		/////////////////////////////////////////
		// Public Application Functions

		/////////////////////////////////////////
		// Protected Application Functions

		protected void ProccessTouch()
		{
			if (Input.GetKey (KeyCode.Mouse0))
			{
				touched = true;

				holdTime += Time.deltaTime;
				debugText_HoldTime.text = DEBUG_HOLD_TEXT + holdTime.ToString ();

				if (holdTime > HOLD_THESHOLD)
				{
					if (TouchHoldAction != null)
						TouchHoldAction (true);
				}
			}
			else
			{
				if (touched)
				{
					if (holdTime < HOLD_THESHOLD)
					{
						if (TouchClickAction != null)
							TouchClickAction ();
					}
					else if (holdTime > HOLD_THESHOLD)
					{
						if (TouchHoldAction != null)
							TouchHoldAction (false);
					}

					holdTime = 0f;
					touched = false;
				}
			}
		}

		/////////////////////////////////////////
		// Event Functions

	}
}
