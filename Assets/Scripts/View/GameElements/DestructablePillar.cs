﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class DestructablePillar : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected GameObject goalPrefab;

		//// Protected
		// References
		protected List<DestructableObstaclePiece> destructablePieces;
		// Primitives
		protected bool firstRightDestroyed = false;

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			destructablePieces = new List<DestructableObstaclePiece> ();
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public DestructablePillar Functions

		public void AddPiece (DestructableObstaclePiece obstaclePiece, bool rightPiece = false)
		{
			if (obstaclePiece == null)
			{
				Debug.LogError ("DestructableObstaclePiece == NULL");
				return;
			}

			if (!destructablePieces.Contains (obstaclePiece))
			{
				destructablePieces.Add (obstaclePiece);
				obstaclePiece.SetRightPiece (rightPiece);
				obstaclePiece.PieceDestroyed += OnPieceDestroyed;
			}
		}

		/////////////////////////////////////////
		// Protected DestructablePillar Functions

		protected bool IsIndexInBounds (int index)
		{
			bool inBounds = false;

			if (index >= 0 || index < destructablePieces.Count)
				inBounds = true;

			return inBounds;
		}

		/////////////////////////////////////////
		// Event Functions

		protected void OnPieceDestroyed (DestructableObstaclePiece piece, bool right)
		{
			if (right)
			{
				if (!firstRightDestroyed)
				{
					firstRightDestroyed = true;

					//Get Others to destroy
					int pieceIndex = destructablePieces.IndexOf (piece);

					if (IsIndexInBounds (pieceIndex - 3))
						destructablePieces [pieceIndex - 3].InstaKill (() =>
						{
							if (IsIndexInBounds (pieceIndex - 2))
								destructablePieces [pieceIndex - 2].InstaKill ();
						});



					if (IsIndexInBounds (pieceIndex + 1))
						destructablePieces [pieceIndex + 1].InstaKill (() =>
						{
							if (IsIndexInBounds (pieceIndex + 2))
								destructablePieces [pieceIndex + 2].InstaKill ();

						});


					//Center Goal Collider with new opening in Pillar
					GameObject goal = Instantiate (goalPrefab, transform);
					Vector3 pieceLocalPos = piece.transform.localPosition;
					pieceLocalPos.x = 0f;
					goal.transform.localPosition = pieceLocalPos;
				}
			}
		}
	}
}
