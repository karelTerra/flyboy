﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class DestructableObstaclePiece : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected int HP = 4;

		[SerializeField]
		protected BoxCollider2D boxCollider2D;

		[SerializeField]
		protected SpriteRenderer spriteRenderer;

		[SerializeField]
		protected new ParticleSystem particleSystem;

		[SerializeField]
		protected AudioSFX audioSFX;

		[Space]
		[SerializeField]
		protected ParticleSystemStopActionTransmitter transmitter;

		[Space]
		[SerializeField]
		protected float shakeTime = 0.5f;

		//// Protected
		// References
		protected ScreenShake screenShake;
		// Primitives
		protected bool isRightPiece;

		// Actions
		public Action<DestructableObstaclePiece, bool> PieceDestroyed;
		protected Action DestoryedParticleSystemStopped;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			transmitter.ParticleSystemStopAction += OnParticleSystemStopped;
			Camera camera = Camera.current;
			if (camera == null)
				Debug.Break ();
			screenShake = camera.GetComponent<ScreenShake> ();
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public DestructablePillarPiece Functions

		public void TakeDamage (int damage = 1)
		{
			HP -= damage;

			if (HP <= 0)
				Destroy ();
		}

		public void InstaKill (Action callback = null)
		{
			Destroy ();
			DestoryedParticleSystemStopped = callback;
		}

		public void SetRightPiece (bool isRightPiece)
		{
			this.isRightPiece = isRightPiece;
		}

		/////////////////////////////////////////
		// Protected DestructablePillarPiece Functions

		protected void Destroy ()
		{
			spriteRenderer.enabled = false;
			boxCollider2D.enabled = false;

			if (PieceDestroyed != null)
				PieceDestroyed (this, isRightPiece);

			//Animation
			particleSystem.Play ();

			//Audio
			audioSFX.PlaySFX ();

			//Camera Shake
			screenShake.StartShake (shakeTime);
		}

		/////////////////////////////////////////
		// Event Functions

		protected void OnParticleSystemStopped ()
		{
			if (DestoryedParticleSystemStopped != null)
				DestoryedParticleSystemStopped ();
		}
	}
}
