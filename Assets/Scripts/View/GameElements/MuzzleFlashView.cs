﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Terra.View
{
	public class MuzzleFlashView : MonoBehaviour
	{

		// Readonly
		protected readonly float DEFAULT_ANGLE_Z = -90f;

		// Enum
		// Struct

		// Serialized

		[SerializeField]
		protected SpriteRenderer spriteRenderer;

		[SerializeField]
		protected float anglePlay;

		[SerializeField]
		protected Color ColorOne;

		[SerializeField]
		protected Color ColorTwo;

		[SerializeField]
		protected AlphaAnimation alphaAnimation;

		//// Protected
		// References
		protected new Transform transform;
		// Primitives

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			this.transform = GetComponent<Transform> ();
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public MuzzleFlashView Functions

		public void Fire ()
		{
			RandomizeColor ();
			RandomizeAngle ();
			alphaAnimation.Antimate ();
		}

		/////////////////////////////////////////
		// Protected MuzzleFlashView Functions

		protected void RandomizeColor ()
		{
			float t = UnityEngine.Random.value;
			spriteRenderer.color = Color.Lerp (ColorOne, ColorTwo, t);
		}

		protected void RandomizeAngle ()
		{
			float dir = UnityEngine.Random.value > 0.5 ? 1f : -1f;
			float offset = anglePlay * UnityEngine.Random.value;

			Vector3 localEuler = transform.localEulerAngles;
			localEuler.z = DEFAULT_ANGLE_Z + (offset * dir);
			transform.localEulerAngles = localEuler;
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
