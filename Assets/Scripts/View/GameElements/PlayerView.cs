﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Terra.Controller;
using System;

namespace Terra.View
{
	public class PlayerView : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized

		[SerializeField]
		protected PlayerController playerController;

		[SerializeField]
		protected GunController gunController;

		[SerializeField]
		protected ParticleSystem engineTapEffect;

		[SerializeField]
		protected ParticleSystem engineGlideEffect;

		//// Protected
		// References
		// Primitives

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			gunController.GunFiringAction += OnHoldAction;
			playerController.TapAction += OnTap;
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public PlayerView Functions

		/////////////////////////////////////////
		// Protected PlayerView Functions

		/////////////////////////////////////////
		// Event Functions

		protected void OnTap ()
		{
			engineTapEffect.Play ();
		}

		protected void OnHoldAction (bool holding)
		{
			if (holding)
			{
				engineGlideEffect.Play ();
			}
			else
			{
				engineGlideEffect.Stop ();
			}
		}
	}
}
