﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Terra.View
{
	public class ParticleSystemStopActionTransmitter : MonoBehaviour 
	{

		// Readonly

		// Enum
		// Struct

		// Serialized

		//// Protected
		// References
		// Primitives

		// Actions
		public Action ParticleSystemStopAction;
		
		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour
		
		void Awake () 
		{
			
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}

		void OnParticleSystemStopped ()
		{
			if (ParticleSystemStopAction != null)
				ParticleSystemStopAction ();
		}

		/////////////////////////////////////////
		// Public ParticleSystemStopActionTransmitter Functions

		/////////////////////////////////////////
		// Protected ParticleSystemStopActionTransmitter Functions

		/////////////////////////////////////////
		// Event Functions
	}
}
