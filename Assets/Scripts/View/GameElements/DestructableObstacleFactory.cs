﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class DestructableObstacleFactory : MonoBehaviour
	{

		// Readonly
		protected readonly float VERT_OFFSET_ROWS = 1.28f;
		protected readonly float HORIZ_OFFSET_CENTER = 0.64f;
		protected readonly float DEFAULT_DRIFT = -3f;
		protected readonly int MIN_DESTRUCTABLE_ROWS = 2;
		protected readonly int TOTOAL_ROWS = 14;
		protected readonly Vector3 PARENT_SCALE = new Vector3 (0.8f, 1f, 1f);

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected Transform spawnPosition;

		[Header ("Prefab")]
		[SerializeField]
		protected GameObject destrucablePillar;

		[Header ("Destructable Components")]
		[SerializeField]
		protected GameObject destWallLeft;
		[SerializeField]
		protected GameObject destWallRight;

		[Header ("NonDestructable Components")]
		[SerializeField]
		protected GameObject lowerCapLeft;
		[SerializeField]
		protected GameObject lowerCapRight;
		[SerializeField]
		protected GameObject upperCapLeft;
		[SerializeField]
		protected GameObject upperCapRight;
		[SerializeField]
		protected GameObject wallLeft;
		[SerializeField]
		protected GameObject wallRight;


		[Header ("Debug")]
		[SerializeField]
		protected bool createObstacle = false;

		//// Protected
		// References
		// Primitives
		protected Vector3 initialBuildHeight
		{
			get
			{
				return new Vector3 (0f, ((TOTOAL_ROWS / 2f) * -VERT_OFFSET_ROWS) + (VERT_OFFSET_ROWS / 2f), 0f);
			}
		}

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{

		}

		void Start ()
		{

		}

		void Update ()
		{
			if (createObstacle)
			{
				BuildObstacle ();
				createObstacle = false;
			}
		}

		/////////////////////////////////////////
		// Public DestructablePillarFactory Functions

		public GameObject CreateDestsructableObstacle()
		{
			return BuildObstacle ();
		}

		/////////////////////////////////////////
		// Protected DestructablePillarFactory Functions

		protected GameObject BuildObstacle ()
		{
			GameObject destPillarGO = Instantiate (destrucablePillar, spawnPosition);

			DestructablePillar destructablePillar = destPillarGO.GetComponent<DestructablePillar> ();
			if (destructablePillar == null)
				Debug.LogError ("This Does not work as expected!");

			//Build Pillar from bottom up
			Vector3 initialHeight = initialBuildHeight;

			for (int i = 0; i < TOTOAL_ROWS; i++)
			{
				for (int j = 0; j < 2; j++)
				{
					float heightOffset = i * VERT_OFFSET_ROWS;

					GameObject destPiece;
					float horizOffset;
					bool right = false;
					if (j % 2 == 0)
					{
						//Even = Left
						destPiece = Instantiate (destWallLeft, destPillarGO.transform);
						horizOffset = -HORIZ_OFFSET_CENTER;
					}
					else
					{
						//Odd = right
						right = true;
						destPiece = Instantiate (destWallRight, destPillarGO.transform);
						horizOffset = HORIZ_OFFSET_CENTER;
					}

					destPiece.transform.localPosition = initialHeight + new Vector3 (horizOffset, heightOffset);
					destructablePillar.AddPiece (destPiece.GetComponent<DestructableObstaclePiece>(), right);
				}
			}

			return destPillarGO;
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
