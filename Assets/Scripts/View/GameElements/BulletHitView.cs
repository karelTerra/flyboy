﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.View
{
	public class BulletHitView : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected ParticleSystemStopActionTransmitter transmitter;

		//// Protected
		// References
		// Primitives

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			if(transmitter != null)
				transmitter.ParticleSystemStopAction += OnStopped;
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public BulletHitView Functions

		/////////////////////////////////////////
		// Protected BulletHitView Functions

		/////////////////////////////////////////
		// Event Functions

		private void OnStopped ()
		{
			GameObject.Destroy (this.gameObject);
		}
	}
}
