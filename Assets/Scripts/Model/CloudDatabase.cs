﻿using UnityEngine;

namespace Terra.Model
{
	// Enums
	public enum CloudType
	{
		Small_White = 1,
		Small_Grey = 2,
		Small_Blue = 4,
		Big_White = 8,
		Big_Grey = 16,
		Big_Blue = 32,
		Huge = 64,
	}

	// Structs
	[System.Serializable]
	public struct Cloud
	{
		public CloudType cloudType;
		public Sprite cloudSprtite;
	}


	[CreateAssetMenu]
	public class CloudDatabase : ScriptableObject
	{

		[SerializeField]
		protected Cloud [] clouds;

		public Sprite GetCloudSprite (CloudType type)
		{
			for (int i = 0; i < clouds.Length; i++)
			{
				if (clouds [i].cloudType == type)
					return clouds [i].cloudSprtite;
			}

			Debug.LogError ("CloudDatabase GetCloudSprite: The cloud type you are trying to retrieve has not been defined. Check the Scriptable Object(CloudSO)");
			return null;
		}

	}
}
