﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Terra.View;
using System;

namespace Terra.Controller
{
	public class GlideController : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected PlayerController playerController;

		[SerializeField]
		protected GlideMeterView glideMeterView;

		[SerializeField]
		protected float equilizeSpeed;
		[SerializeField]
		protected float glideTime;

		[SerializeField]
		protected float glideCooldown;

		[SerializeField]
		protected AudioSFX coolDownCompleteSFX;

		//// Protected
		// References
		// Primitives
		protected float glideTimer;
		protected bool gliding = false;
		protected bool glideReady = true;
		protected bool glideTriggerdAction = false;
		protected Coroutine glideCooldownRoutine;

		// Actions
		public Action<bool> GlidingAction;

		// Properties
		public float GlideTime { get { return glideTime; } }
		public float GlideTimer { get { return glideTimer; } }

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			playerController.CheckHoldFunc = CanActivateGlide;
			playerController.HoldActiveAction += OnPlayerHoldAction;
		}

		void Start ()
		{

		}

		void Update ()
		{
			if (gliding)
				Glide ();
		}

		/////////////////////////////////////////
		// Public GlideController Functions

		/////////////////////////////////////////
		// Protected GlideController Functions

		protected bool CanActivateGlide ()
		{
			return glideReady;
		}

		protected void Glide ()
		{
			glideTimer += Time.deltaTime;
			if (glideTimer >= glideTime)
			{
				gliding = false;
				Cooldown ();
				return;
			}

			float playerVerticalSpeed = playerController.VerticalSpeed;
			float invertedSign = -Mathf.Sign (playerVerticalSpeed);

			if (Mathf.Abs (playerVerticalSpeed) < 0.4f)
			{
				if (!glideTriggerdAction && GlidingAction != null)
				{
					GlidingAction (true);
					glideTriggerdAction = true;
				}

				playerController.SetVerticalVelocity (0f);
			}
			else
				playerController.AddVerticalForce (equilizeSpeed * invertedSign);

			glideMeterView.SetFill ((glideTime - glideTimer) / glideTime);
		}

		protected void Cooldown ()
		{
			if (glideCooldownRoutine == null)
				glideCooldownRoutine = StartCoroutine (CooldownRoutine ());
		}

		protected IEnumerator CooldownRoutine ()
		{
			glideMeterView.SetViewStateActive (false);

			glideReady = false;

			if (GlidingAction != null)
				GlidingAction (false);
			glideTriggerdAction = false;

			float t = 0;
			while (t < glideCooldown)
			{
				t += Time.deltaTime;
				glideMeterView.SetFill (t / glideCooldown);
				yield return null;
			}

			glideMeterView.SetFill (1f);
			glideMeterView.SetViewStateActive (true);

			coolDownCompleteSFX.PlaySFX ();
			glideMeterView.PlayIconAnimation ();

			glideCooldownRoutine = null;
			glideReady = true;
		}

		/////////////////////////////////////////
		// Event Functions

		protected void OnPlayerHoldAction (bool active)
		{
			if (active)
			{
				glideTimer = 0f;
				gliding = true;
			}
			else
			{
				if (gliding)
				{
					gliding = false;
					Cooldown ();
				}
			}
		}
	}
}
