﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Terra.View;
using Terra.Interface;
using System.Linq;

namespace Terra.Controller
{
	public class GameController : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected PlayerController playerController;

		[SerializeField]
		protected GameScroll gameScroll;
		[Space]
		[SerializeField]
		protected PositionAnimation playerAnimation;
		[SerializeField]
		protected PositionAnimation airplaneAnimation;

		[Space]
		[SerializeField]
		protected ObstacleController obstacleController;
		[SerializeField]
		protected TutorialController tutorialController;


		[Space]
		[SerializeField]
		protected GameUIManager gameUIManager;
		[SerializeField]
		protected ScoreView scoreView;

		[Space]
		[SerializeField]
		protected Collision2DReporter playerCollisionReporter;
		[SerializeField]
		protected Collision2DReporter playerCollisionDestructableReporter;

		[SerializeField]
		protected Collision2DReporter waterCollisionReporter;

		[Space]
		[SerializeField]
		protected CanvasGroupAnimation screenFade;

		[Space]
		[SerializeField]
		protected AudioController audioController;

		[Space]
		[Header ("DUBUG")]
		[SerializeField]
		protected bool alwaysTutorial = false;

		//// Protected
		// References
		protected List<IResetable> resetableScriptsList;
		// Primitives
		protected bool gameOver = false;

		// Actions
		public Action ReturnToMenuAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			gameUIManager.BackToMenuAction += OnBackToMenuSelected;
			gameUIManager.PauseSelectedAction += OnPauseSelected;
			gameUIManager.ResumeSelectedAction += OnResumeSelected;
			gameUIManager.RestartSelectedAction += OnRestartSelected;

			playerCollisionReporter.CollisionAction += OnPlayerHitObstacle;
			playerCollisionDestructableReporter.CollisionAction += OnPlayerHitObstacle;
			waterCollisionReporter.CollisionReportAction += OnPlaneHitWater;

			scoreView.GameOver = IsGameOver;
			scoreView.TutorialComplete = IsTutorialComplete;

			resetableScriptsList = new List<IResetable> ();

			var mObjs = GameObject.FindObjectsOfType<MonoBehaviour> ();
			IResetable [] interfaceScripts = (from a in mObjs where a.GetType ().GetInterfaces ().Any (k => k == typeof (IResetable)) select (IResetable)a).ToArray ();
			resetableScriptsList.AddRange (interfaceScripts);
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public GameController Functions

		public void StartGame ()
		{
			gameScroll.SpeedUp ();
			airplaneAnimation.Reset ();
			audioController.FadeInPlayer ();
			airplaneAnimation.Animate ();
			playerAnimation.Animate (OnPlayerInControl, 0.15f);
		}

		/////////////////////////////////////////
		// Protected GameController Functions

		protected void PauseGame ()
		{
			playerController.PausePlayer ();
			gameScroll.PauseScroll ();
			obstacleController.PauseSpawning ();
		}

		protected void ResumeGame ()
		{
			playerController.ActivatePlayer ();
			gameScroll.ResumeScroll ();
			obstacleController.ResumeSpawning ();
		}

		protected void GameOver ()
		{
			if (!gameOver)
			{
				gameOver = true;
				gameScroll.SlowDown ();
				playerController.GameOver ();
				obstacleController.StopSpawning ();
				gameUIManager.ShowGameOverMenu ();
				audioController.PlayGameOver ();

				if (!IsTutorialComplete ())
				{
					tutorialController.Reset ();
				}
			}
		}

		protected void ResetGame ()
		{
			foreach (var resetable in resetableScriptsList)
				resetable.Reset ();

			gameOver = false;
		}

		protected bool IsGameOver ()
		{
			return gameOver;
		}

		protected bool IsTutorialComplete ()
		{
			return PlayerPrefs.GetInt ("TutorialComplete", 0) != 0;
		}

		protected void BeginTutorial ()
		{
			tutorialController.TutorialCompleteAction += OnTutorialComplete;
			tutorialController.BeginTutorial ();
		}

		protected void BeginGame ()
		{
			obstacleController.StartSpawning ();
			gameUIManager.Reset ();
			gameUIManager.ShowGameUI ();
		}

		/////////////////////////////////////////
		// Event Functions

		protected void OnBackToMenuSelected ()
		{
			audioController.FadeOut ();
			screenFade.FadeIn (() =>
			{
				if (ReturnToMenuAction != null)
					ReturnToMenuAction ();
			});

		}

		protected void OnPauseSelected ()
		{
			PauseGame ();
			audioController.FadeToPauseVolume ();
		}

		protected void OnResumeSelected ()
		{
			ResumeGame ();
			audioController.FadeFromPauseVolume ();
		}

		protected void OnRestartSelected ()
		{
			ResetGame ();
			StartGame ();
		}

		protected void OnPlayerHitObstacle ()
		{
			GameOver ();
		}

		protected void OnPlaneHitWater (Collider2D obj)
		{
			if (!gameOver)
				GameOver ();

			playerController.UpdatePlayer = false;
			audioController.FadeOutPlayer ();
		}

		protected void OnPlayerInControl ()
		{
			playerController.ActivatePlayer ();

			if (PlayerPrefs.GetInt ("TutorialComplete", 0) == 0 || alwaysTutorial)
			{
				BeginTutorial ();
			}
			else
			{
				BeginGame ();
			}
		}

		protected void OnTutorialComplete ()
		{
			BeginGame ();
		}
	}
}
