﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Terra.View;
using Terra.Interface;

namespace Terra.Controller
{
	public class AudioController : MonoBehaviour, IResetable
	{

		// Readonly
		protected readonly float AUDIO_GROUP_MUSIX_VOLUME = -10f;
		protected readonly float AUDIO_GROUP_SFX_VOLUME = -15f;

		// Enum
		// Struct

		// Serialized
		[Header ("Mixer")]
		[SerializeField]
		protected AudioMixer masterMixer;

		[Header ("Music")]
		[SerializeField]
		protected AudioSource menuMusic;
		[SerializeField]
		protected AudioSource gameMusic;

		[Header ("Ambient")]
		[SerializeField]
		protected AudioSource ambientSound;

		[Header ("Player")]
		[SerializeField]
		protected AudioSource playerEngine;

		[Header ("GameOver")]
		[SerializeField]
		protected AudioSFX gameOverSFX;

		[Header ("FX")]
		[SerializeField]
		protected AudioSource sfx;

		//// Protected
		// References
		// Primitives

		// Actions

		// Properties
		protected bool MusicMuted
		{
			get
			{
				return PlayerPrefs.GetInt ("MusicMuted", 0) != 0;
			}
		}
		protected bool SFXMuted
		{
			get
			{
				return PlayerPrefs.GetInt ("FXMuted", 0) != 0;
			}
		}

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{

		}

		void Start ()
		{

		}

		void Update ()
		{

		}


		/////////////////////////////////////////
		// Public AudioController Functions

		public void Reset ()
		{
			SetSFXMuted (SFXMuted);
			SetMusicMuted (MusicMuted);
			ResetPlayerEngine ();
		}

		public void PlayMenuMusic ()
		{
			menuMusic.Play ();
			ambientSound.Play ();
		}

		public void FadeIn ()
		{
			StartCoroutine (FadeRoutine (GetUnMutedSources (), 1.95f));
		}

		public void ResetPlayerEngine ()
		{
			playerEngine.volume = 0f;
		}

		public void FadeInPlayer ()
		{
			if (!SFXMuted)
				StartCoroutine (FadeRoutine (new AudioSource [] { playerEngine }, .5f, 0f, 0.3f));
		}

		public void FadeOutPlayer ()
		{
			if (!SFXMuted)
				StartCoroutine (FadeRoutine (new AudioSource [] { playerEngine }, 0.05f, 1f, 0f));
		}

		public void FadeOut ()
		{
			StartCoroutine (FadeRoutine (GetUnMutedSources (), 0.45f, 1f, 0f));
		}

		public void CrossFadeToGame ()
		{
			if (!MusicMuted)
				StartCoroutine (CrossFadeRoutine (menuMusic, gameMusic, 0.5f));
		}

		public void FadeToPauseVolume ()
		{
			StartCoroutine (FadeRoutine (new AudioSource [] { gameMusic, ambientSound }, 0.5f, 1f, 0.5f));
		}

		public void FadeFromPauseVolume ()
		{
			StartCoroutine (FadeRoutine (new AudioSource [] { gameMusic, ambientSound }, 0.5f, 0.5f, 1f));
		}

		public void SetMusicMuted (bool muted)
		{
			float volume = muted ? -80f : AUDIO_GROUP_MUSIX_VOLUME;

			masterMixer.SetFloat ("MusicVolume", volume);
		}

		public void SetSFXMuted (bool muted)
		{
			float volume = muted ? -80f : AUDIO_GROUP_SFX_VOLUME;

			masterMixer.SetFloat ("SFXVolume", volume);
		}

		public void PlayGameOver ()
		{
			gameOverSFX.PlaySFX ();
		}

		/////////////////////////////////////////
		// Protected AudioController Functions

		protected IEnumerator FadeRoutine (AudioSource [] audioSources, float fadeTime, float startVolume = 0f, float endVolume = 1f)
		{
			float t = 0f;
			float volume = startVolume;

			while (t < fadeTime)
			{
				volume = Mathf.Lerp (startVolume, endVolume, t / fadeTime);

				SetAudioSourcesVolume (audioSources, volume, endVolume > startVolume);

				t += Time.deltaTime;
				yield return null;
			}
			SetAudioSourcesVolume (audioSources, endVolume, endVolume > startVolume);
		}

		protected IEnumerator CrossFadeRoutine (AudioSource currentSource, AudioSource newSource, float fadeTime)
		{
			float t = 0f;
			float volumeCurrent = 0f;
			float volumeNew = 0f;

			gameMusic.Play ();

			while (t < fadeTime)
			{
				volumeCurrent = 1f - (t / fadeTime);
				volumeNew = t / fadeTime;

				currentSource.volume = volumeCurrent;
				newSource.volume = volumeNew;

				t += Time.deltaTime;
				yield return null;
			}

			currentSource.volume = 0f;
			newSource.volume = 1f;

			menuMusic.Stop ();
		}

		protected void SetAudioSourcesVolume (AudioSource [] audioSources, float volume, bool up = true)
		{
			for (int i = 0; i < audioSources.Length; i++)
			{
				if(up && volume > audioSources [i].volume)
					audioSources [i].volume = volume;
				else if(volume < audioSources [i].volume)
					audioSources [i].volume = volume;
			}
		}

		protected AudioSource [] GetUnMutedSources ()
		{
			List<AudioSource> sources = new List<AudioSource> ();

			if (!SFXMuted)
			{
				sources.Add (sfx);

				if (App.Application.GameState == App.Application.State.Game && playerEngine.volume > 0f)
					sources.Add (playerEngine);
			}
			if (!MusicMuted)
			{
				if (App.Application.GameState == App.Application.State.Menu)
					sources.Add (menuMusic);
				else
					sources.Add (gameMusic);

				sources.Add (ambientSound);
			}

			return sources.ToArray ();
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
