﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Terra.View;

namespace Terra.Controller
{
	public class GunController : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[Header ("Gun Trigger")]
		[SerializeField]
		protected GlideController glideController;

		[Header ("Gun Properties")]

		[SerializeField]
		protected float fireRate;

		[Header ("Gun Audio")]

		[SerializeField]
		protected AudioSource barrel1;

		[SerializeField]
		protected AudioSource barrel2;

		[SerializeField]
		protected AudioClip gunShot;

		[SerializeField]
		protected float pitchMin;

		[SerializeField]
		protected float pitchMax;

		[Header ("GunView")]

		[SerializeField]
		protected MuzzleFlashView muzzleFlashView;

		[SerializeField]
		protected GameObject bulletHitGO;


		[Header ("Gun Physics")]

		[SerializeField]
		protected Transform bulletRayOrigin;
		[SerializeField]
		protected LayerMask bulletCollisionMask;


		//// Protected
		// References
		protected Coroutine fireRoutine;
		// Primitives
		protected bool fire;

		// Actions
		public Action<bool> GunFiringAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			glideController.GlidingAction += OnTriggerAction;
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public GunController Functions

		/////////////////////////////////////////
		// Protected GunController Functions
		protected void Fire ()
		{
			fireRoutine = StartCoroutine (FireRoutine ());
		}

		protected IEnumerator FireRoutine ()
		{
			if (GunFiringAction != null)
				GunFiringAction (true);

			while (fire)
			{
				FireBarrel (barrel1);
				yield return new WaitForSeconds (1f / fireRate);
				FireBarrel (barrel2);
				yield return new WaitForSeconds (1f / fireRate);
			}

			if (GunFiringAction != null)
				GunFiringAction (false);

			fireRoutine = null;
		}

		protected void FireBarrel (AudioSource barrelAudioSource)
		{
			//physics
			RaycastHit2D [] hit = new RaycastHit2D [1];
			ContactFilter2D contactFilter2D = new ContactFilter2D ();
			contactFilter2D.SetLayerMask (bulletCollisionMask);
			contactFilter2D.useTriggers = true;
			if (Physics2D.Raycast (bulletRayOrigin.position, transform.right, contactFilter2D, hit) > 0)
			{
				GameObject.Instantiate (bulletHitGO, hit [0].point, Quaternion.identity, hit [0].collider.transform);

				//Hit Destructable Pieces
				DestructableObstaclePiece destructableObstaclePiece = hit [0].transform.GetComponent<DestructableObstaclePiece> ();
				if (destructableObstaclePiece != null)
					destructableObstaclePiece.TakeDamage ();
			}

			//audio
			barrelAudioSource.pitch = UnityEngine.Random.Range (pitchMin, pitchMax);
			barrelAudioSource.PlayOneShot (gunShot);

			//animation

			//Muzzle Flach
			muzzleFlashView.Fire ();
		}
		/////////////////////////////////////////
		// Event Functions

		protected void OnTriggerAction (bool triggered)
		{
			if (triggered)
			{
				fire = true;
				if (fireRoutine == null)
					fireRoutine = StartCoroutine (FireRoutine ());
			}
			else
			{
				fire = false;
			}
		}

		/////////////////////////////////////////
		// Gizmos Functions

		private void OnDrawGizmos ()
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawLine (bulletRayOrigin.position, (transform.right * 20f));
		}
	}
}
