﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Terra.View;
using UnityEngine.UI;

namespace Terra.Controller
{
	public class MenuController : MonoBehaviour
	{

		// Readonly
		private readonly float MENU_STAGGERED_FADE_DELAY = 0.5f;
		private readonly float MENU_DRIFT_FADE_DELAY = 0.5f;

		// Enum
		protected enum State
		{
			Title,
			Play,
			Settings,
		}
		// Struct

		// Serialized
		[SerializeField]
		protected CanvasGroupAnimation title;
		[SerializeField]
		protected CanvasGroupAnimation play;
		[SerializeField]
		protected CanvasGroupAnimation settings;

		[Space]
		[SerializeField]
		protected Button playButton;
		//[SerializeField]
		//protected Button settingsButton;

		[Space]
		[SerializeField]
		protected CanvasGroupAnimation screenFade;

		[Space]
		[SerializeField]
		protected AudioController audioController;



		//// Protected
		// References
		// Primitives

		// Actions
		public Action PlaySelectedAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			playButton.onClick.AddListener (() => OnPlaySelected ());
		}

		void Start ()
		{
			screenFade.FadeOut (true, ShowMenu);
			audioController.Reset ();
			audioController.PlayMenuMusic ();
			audioController.FadeIn ();
		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public MenuController Functions

		public void ShowMenu ()
		{
			title.FadeIn ();
			play.FadeIn (null, MENU_STAGGERED_FADE_DELAY);
			settings.FadeIn (null, MENU_STAGGERED_FADE_DELAY);
		}

		public void HideMenu ()
		{
			title.FadeOut (false, null /*reset position*/, MENU_DRIFT_FADE_DELAY);
			play.FadeOut (false, null /*reset position*/, MENU_DRIFT_FADE_DELAY);
			settings.FadeOut ();
		}

		/////////////////////////////////////////
		// Protected MenuController Functions

		/////////////////////////////////////////
		// Event Functions

		protected void OnPlaySelected ()
		{
			HideMenu ();

			if (PlaySelectedAction != null)
				PlaySelectedAction ();
		}
	}
}
