﻿using System;
using System.Collections;
using Terra.View;
using Terra.Interface;
using UnityEngine;

namespace Terra.Controller
{
	public class TutorialController : MonoBehaviour, IResetable
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected ObstacleController obstacleController;

		[SerializeField]
		protected Collision2DReporter goalCollisionReporter;

		[Space]
		[SerializeField]
		protected CanvasGroupAnimation tapCanvasAnimation;

		[SerializeField]
		protected CanvasGroupAnimation glideCanvasAnimation;

		[SerializeField]
		protected CanvasGroupAnimation glideMeterAnimation;

		//// Protected
		// References
		// Primitives
		bool tapTutorial = false;
		bool glideTutorial = false;

		// Actions
		public Action TutorialCompleteAction;

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{

		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		public void Reset ()
		{
			tapTutorial = false;
			glideTutorial = false;
		}

		/////////////////////////////////////////
		// Public TutorialController Functions

		public void BeginTutorial ()
		{
			tapTutorial = true;
			ListenToGoalReporter ();
			tapCanvasAnimation.FadeOut (true, null, 0.5f);
			obstacleController.SpawnObstacle (true);
		}

		/////////////////////////////////////////
		// Protected TutorialController Functions

		protected void BeginGlideTutorial ()
		{
			StartCoroutine (Wait (1f, () =>
			{
				 glideTutorial = true;
				 ListenToGoalReporter ();
				 glideCanvasAnimation.FadeOut (true, null, 0.5f);
				 glideMeterAnimation.FadeIn ();
				 obstacleController.SpawnObstacle (false);
			}));
		}

		protected void CompleteTutorial ()
		{
			PlayerPrefs.SetInt ("TutorialComplete", 1);

			if (TutorialCompleteAction != null)
				TutorialCompleteAction ();
		}

		protected void ListenToGoalReporter ()
		{
			goalCollisionReporter.CollisionAction += OnGoalReported;
		}

		protected void StopListeningToGoalReporter ()
		{
			goalCollisionReporter.CollisionAction -= OnGoalReported;
		}

		protected IEnumerator Wait (float time, Action callback)
		{
			yield return new WaitForSeconds (time);

			if (callback != null)
				callback ();
		}

		/////////////////////////////////////////
		// Event Functions

		protected void OnGoalReported ()
		{
			if (tapTutorial)
			{
				tapTutorial = false;
				StopListeningToGoalReporter ();

				BeginGlideTutorial ();
			}
			else if (glideTutorial)
			{
				glideTutorial = false;
				StopListeningToGoalReporter ();

				CompleteTutorial ();
			}

		}
	}
}
