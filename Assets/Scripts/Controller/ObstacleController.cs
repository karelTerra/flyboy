﻿using UnityEngine;
using Terra.View;
using Terra.Interface;

namespace Terra.Controller
{
	public class ObstacleController : MonoBehaviour, IResetable
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected GameObject ObstacelPrefab;

		[SerializeField]
		protected DestructableObstacleFactory destructableObstacleFactory;

		[Header ("Spawning")]
		[SerializeField]
		protected float rate;
		[SerializeField]
		protected float maxHeight;
		[SerializeField]
		protected float minHeight;
		[SerializeField]
		protected Transform spawnPosition;
		[Space]
		[SerializeField]
		protected bool spawnOnStart;

		[Header ("Scroll")]
		[SerializeField]
		protected GameScroll gameScroll;

		//// Protected
		// References
		protected new Transform transform;
		protected Coroutine coroutine;
		// Primitives
		protected bool hasStartedSpawning;
		protected bool isSpawning;

		protected float spawnRate;
		protected float time;

		protected int spawnCount;

		// Actions

		// Properties
		public bool HasStartedSpawning { get { return hasStartedSpawning; } set { hasStartedSpawning = value; } }


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		private void Awake ()
		{
			transform = gameObject.transform;
			gameScroll.ScrollLerpTChangedAction += OnScrollTValChanged;
			spawnRate = 1f / rate;
		}

		void Start ()
		{
			if (spawnOnStart)
			{
				StartSpawning ();
			}
		}

		void Update ()
		{
			if (isSpawning)
			{
				time += Time.deltaTime;

				if (time >= spawnRate)
				{
					time -= spawnRate;

					bool basic = true;
					if (spawnCount % 3 == 0)
						basic = false;
					SpawnObstacle (basic);
				}
			}
		}

		public void SpawnObstacle (bool basic = true)
		{
			spawnCount++;

			GameObject go;

			if (basic)
			{
				go = Instantiate (ObstacelPrefab, spawnPosition);

				Vector3 position = Vector3.zero;
				position.y = Mathf.Lerp (minHeight, maxHeight, UnityEngine.Random.value);
				go.transform.localPosition = position;
			}
			else
			{
				go = destructableObstacleFactory.CreateDestsructableObstacle ();
			}

			HorizontalDrift drift = go.GetComponent<HorizontalDrift> ();
			if (drift != null)
				drift.SetDriftEnabled (true);
		}

		/////////////////////////////////////////
		// Public ObstacleController Functions

		public void StartSpawning ()
		{
			time = spawnRate;
			hasStartedSpawning = true;
			SetIsSpawningEnabled (true);
		}

		public void ResumeSpawning ()
		{
			SetIsSpawningEnabled (true);
		}

		public void StopSpawning ()
		{
			SetIsSpawningEnabled (false);
		}

		public void PauseSpawning ()
		{
			SetIsSpawningEnabled (false);
		}

		public void Reset ()
		{
			for (int i = spawnPosition.childCount - 1; i >= 0; i--)
			{
				hasStartedSpawning = false;
				GameObject.Destroy (spawnPosition.GetChild (i).gameObject);
			}
		}

		public void SetObstacleSpeed (float speed)
		{
			for (int i = spawnPosition.childCount - 1; i >= 0; i--)
			{
				spawnPosition.GetChild (i).GetComponent<HorizontalDrift> ().SetVelocity (speed);
			}
		}

		/////////////////////////////////////////
		// Protected ObstacleController Functions

		protected void SetIsSpawningEnabled (bool enabled)
		{
			isSpawning = enabled;
		}

		/////////////////////////////////////////
		// Event Functions

		private void OnScrollTValChanged (float t)
		{
			//if (hasStartedSpawning)
			//{
				SetObstacleSpeed (Mathf.Lerp (0f, -3f, t));
			//}
		}
	}
}
