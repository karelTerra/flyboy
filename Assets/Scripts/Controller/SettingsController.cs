﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Terra.View;

namespace Terra.Controller
{
	public class SettingsController : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized
		[Header ("UI")]
		[SerializeField]
		protected Button settingsButton;

		[Header ("Menu")]
		[SerializeField]
		protected CanvasGroupAnimation settingsMenuCanvasGroupAnimation;
		[SerializeField]
		protected Button muteMusicButton;
		[SerializeField]
		protected UIImageButtonToggleView musicToggleView;
		[SerializeField]
		protected Button muteFXButton;
		[SerializeField]
		protected UIImageButtonToggleView fxToggleView;
		[SerializeField]
		protected Button closeMenuButton;

		//// Protected
		// References
		// Primitives

		// Actions
		public Action<bool> MusicMutedAction;
		public Action<bool> FXMutedAction;

		// Properties
		protected bool MusicMuted
		{
			get
			{
				return PlayerPrefs.GetInt ("MusicMuted", 0) != 0;
			}
			set
			{
				int val = value ? 1 : 0;
				PlayerPrefs.SetInt ("MusicMuted", val);
			}
		}
		protected bool FXMuted
		{
			get
			{
				return PlayerPrefs.GetInt ("FXMuted", 0) != 0;
			}
			set
			{
				int val = value ? 1 : 0;
				PlayerPrefs.SetInt ("FXMuted", val);
			}
		}

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			settingsButton.onClick.AddListener (OnSettingsSelected);
			closeMenuButton.onClick.AddListener (OnCloseMenuSelected);

			muteMusicButton.onClick.AddListener (OnMusicMuteSelected);
			muteFXButton.onClick.AddListener (OnFXMuteSelected);

			musicToggleView.SetToggle (!MusicMuted);
			fxToggleView.SetToggle (!FXMuted);
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public SettingsController Functions

		/////////////////////////////////////////
		// Protected SettingsController Functions

		/////////////////////////////////////////
		// Event Functions

		protected void OnSettingsSelected ()
		{
			settingsMenuCanvasGroupAnimation.FadeIn ();
		}

		protected void OnCloseMenuSelected ()
		{
			settingsMenuCanvasGroupAnimation.FadeOut ();
		}

		protected void OnFXMuteSelected ()
		{
			FXMuted = !FXMuted;

			if (FXMutedAction != null)
				FXMutedAction (FXMuted);
		}

		protected void OnMusicMuteSelected ()
		{
			MusicMuted = !MusicMuted;

			if (MusicMutedAction != null)
				MusicMutedAction (MusicMuted);
		}
	}
}
