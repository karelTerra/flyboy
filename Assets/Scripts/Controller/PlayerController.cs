﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Terra.View;
using Terra.Interface;
using System;

namespace Terra.Controller
{
	public class PlayerController : MonoBehaviour, IResetable
	{

		// Readonly
		private readonly string DEBUG_SPEED_TEXT = "SPEED: ";

		// Enum
		// Struct

		// Serialized
		[SerializeField]
		protected TouchInput touchInput;
		[SerializeField]
		protected RotationFromSpeed rotationFromSpeed;
		[SerializeField]
		protected AudioPitchFromSpeed audioPitchFromSpeed;
		[SerializeField]
		protected GlideController glideController;

		[Header ("Handeling Properties")]
		[SerializeField]
		protected float gravity;
		[SerializeField]
		protected float bounceForce;

		[Space]
		[SerializeField]
		protected Collision2DReporter skyCollisionReporter;

		[Header ("DEBUG")]
		[SerializeField]
		protected Text debugText_Speed;

		//// Protected
		// References
		protected new Transform transform;
		// Primitives
		protected Vector3 origPosition;
		protected Vector3 velocity;
		protected bool onDown = false;
		protected bool holding = false;

		protected bool playerHasControl = false;
		protected bool updatePlayer = false;
		protected bool applyGravity = true;

		// Actions/Func
		public Func<bool> CheckHoldFunc;
		public Action<bool> HoldActiveAction;
		public Action TapAction;

		// Properties
		public float VerticalSpeed { get { return velocity.y; } }
		public bool UserHasControl { get { return playerHasControl; } set { playerHasControl = value; } }
		public bool UpdatePlayer { get { return updatePlayer; } set { updatePlayer = value; } }
		public bool IgnoreNextClick { get; set; }


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		private void Awake ()
		{
			this.transform = gameObject.transform;
			origPosition = transform.position;

			touchInput.TouchClickAction += OnClick;
			touchInput.TouchHoldAction += GlideOnHold;

			glideController.GlidingAction += OnGliding;

			skyCollisionReporter.CollisionDirAction += OnHitSkyAction;

			ResetPlayer ();
		}

		void Start ()
		{

		}

		private void Update ()
		{
			if (updatePlayer)
			{
				if (applyGravity)
					velocity.y += gravity * Time.deltaTime;

				transform.position += velocity * Time.deltaTime;

				rotationFromSpeed.SetRotation (VerticalSpeed);
				audioPitchFromSpeed.SetPitch (Mathf.Abs (VerticalSpeed));
			}
		}

		public void Reset ()
		{
			ResetPlayer ();
		}

		/////////////////////////////////////////
		// Public PlayerController Functions

		public void PausePlayer ()
		{
			playerHasControl = false;
			updatePlayer = false;
		}

		public void ActivatePlayer ()
		{
			playerHasControl = true;
			updatePlayer = true;
		}

		public void GameOver ()
		{
			playerHasControl = false;

			if (holding)
			{
				HoldComplete ();
			}
		}

		public void ResetPlayer ()
		{
			transform.position = origPosition;
			velocity = Vector3.zero;
			rotationFromSpeed.SetRotation (VerticalSpeed);
			audioPitchFromSpeed.SetPitch (Mathf.Abs (VerticalSpeed));
			playerHasControl = false;
			updatePlayer = false;
		}

		public void AddVerticalForce (float force)
		{
			velocity.y += force;
		}

		public void SetVerticalVelocity (float vel)
		{
			velocity.y = vel;
		}

		/////////////////////////////////////////
		// Protected PlayerController Functions

		protected void OnClick ()
		{
			if (IgnoreNextClick)
			{
				IgnoreNextClick = false;
				return;
			}

			if (playerHasControl)
			{
				float extraForce = 0f;
				if (velocity.y >= bounceForce * 0.7f)
				{
					extraForce = bounceForce * 0.5f;
				}

				velocity.y = bounceForce + extraForce;

				if (TapAction != null)
					TapAction ();
			}
		}

		protected void GlideOnHold (bool down)
		{
			if (playerHasControl)
			{
				if (down)
				{
					HoldBegin ();
				}
				else
				{
					if (holding)
						HoldComplete ();
				}
			}
		}

		protected void HoldBegin ()
		{
			holding = true;
			if (!onDown)
			{
				onDown = true;

				if (CheckHoldFunc ())
				{
					applyGravity = false;

					if (HoldActiveAction != null)
						HoldActiveAction (true);
				}
			}
		}

		protected void HoldComplete ()
		{
			if(holding)
			{
				if (HoldActiveAction != null)
					HoldActiveAction (false);

				applyGravity = true;
				onDown = false;
				holding = false;
			}
		}


		protected void OnGliding (bool gliding)
		{
			if (!gliding)
				HoldComplete ();
		}

		/////////////////////////////////////////
		// Event Functions

		protected void OnHitSkyAction (bool enter)
		{
			if(enter)
			{
				playerHasControl = false;
			}
			else
			{
				playerHasControl = true;
			}

		}
	}
}
