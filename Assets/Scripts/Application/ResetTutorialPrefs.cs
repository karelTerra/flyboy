﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra.App
{
	public class ResetTutorialPrefs : MonoBehaviour
	{

		// Readonly

		// Enum
		// Struct

		// Serialized

		//// Protected
		// References
		// Primitives

		// Actions

		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{

		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public PlayerPrefsMan Functions
		
		public void ResetTutorial ()
		{
			Debug.Log ("TUTORIAL RESET");

			PlayerPrefsUtility.ClearPlayerPref ("TutorialComplete");
		}

		/////////////////////////////////////////
		// Protected PlayerPrefsMan Functions

		/////////////////////////////////////////
		// Event Functions
	}
}
