﻿
using UnityEngine;
using Terra.Controller;
using UnityEngine.SceneManagement;

namespace Terra.App
{
	public class Application : MonoBehaviour
	{

		// Readonly

		// Enum
		public enum State
		{
			Menu,
			Game,
		}
		// Struct

		// Serialized
		[SerializeField]
		protected MenuController menuController;

		[SerializeField]
		protected GameController gameController;

		[SerializeField]
		protected SettingsController settingsController;

		[SerializeField]
		protected AudioController audioController;

		//// Protected
		// References
		// Primitives
		protected static State gameState = State.Menu;

		// Actions
		public static State GameState
		{
			get
			{
				return gameState;
			}
		}

		// Properties


		/////////////////////////////////////////
		// Inherited from MonoBehaviour

		void Awake ()
		{
			menuController.PlaySelectedAction += OnPlaySelected;
			gameController.ReturnToMenuAction += OnReturnToMenuSelected;

			settingsController.MusicMutedAction += OnMusicMuted;
			settingsController.FXMutedAction += OnFXMuted;
		}

		void Start ()
		{

		}

		void Update ()
		{

		}

		/////////////////////////////////////////
		// Public Application Functions

		/////////////////////////////////////////
		// Protected Application Functions

		/////////////////////////////////////////
		// Event Functions

		protected void OnPlaySelected ()
		{
			// Transition into Game
			gameController.StartGame ();
			audioController.CrossFadeToGame ();
			gameState = State.Game;
		}

		protected void OnReturnToMenuSelected ()
		{
			// Back to menu
			SceneManager.LoadScene (0);
			gameState = State.Menu;

		}

		protected void OnFXMuted (bool muted)
		{
			audioController.SetSFXMuted (muted);
		}

		protected void OnMusicMuted (bool muted)
		{
			audioController.SetMusicMuted (muted);
		}
	}
}
