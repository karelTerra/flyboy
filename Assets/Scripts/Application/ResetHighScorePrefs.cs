﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra
{
	public class ResetHighScorePrefs : MonoBehaviour 
	{

		// Readonly
		
		// Enum
		// Struct

		// Serialized
		
		//// Protected
		// References
		// Primitives
		
		// Actions
		
		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour
		
		void Awake () 
		{
			
		}

		void Start () 
		{
			
		}
		
		void Update () 
		{
			
		}
		
		/////////////////////////////////////////
		// Public ResetHighScorePrefs Functions

		public void ResetHighScore ()
		{
			Debug.Log ("RESET HIGH SCORE!");

			PlayerPrefsUtility.ClearPlayerPref ("Best");
		}
		
		/////////////////////////////////////////
		// Protected ResetHighScorePrefs Functions
		
		/////////////////////////////////////////
		// Event Functions
	}
}
