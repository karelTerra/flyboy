﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra
{
	public class FizzBuzz : MonoBehaviour 
	{

		// Readonly
		
		// Enum
		// Struct

		// Serialized
		
		//// Protected
		// References
		// Primitives
		
		// Actions
		
		// Properties

		/////////////////////////////////////////
		// Inherited from MonoBehaviour
		
		void Awake () 
		{
			
		}

		void Start () 
		{
			PrintFizzBuzz ();
		}
		
		void Update () 
		{
			
		}
		
		/////////////////////////////////////////
		// Public FizzBuzz Functions
		
		/////////////////////////////////////////
		// Protected FizzBuzz Functions
		
		protected void PrintFizzBuzz ()
		{
			for (int i = 1; i <= 100; i++)
			{
				if (i % 3 == 0 && i % 5 == 0)
				{
					Debug.Log ("FizzBuzz");
					continue;
				}
				else if(i % 3 == 0)
				{
					Debug.Log ("Fizz");
					continue;
				}
				else if (i % 5 == 0)
				{
					Debug.Log ("Buzz");
					continue;
				}
				else
				{
					Debug.Log (i.ToString ());
				}

			}
		}

		/////////////////////////////////////////
		// Event Functions
	}
}
