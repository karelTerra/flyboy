﻿namespace Terra.Interface
{
	public interface IResetable 
	{
		void Reset ();
	}
}
