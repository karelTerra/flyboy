﻿
// Initial Concept by http://www.reddit.com/user/zaikman
// Revised by http://www.reddit.com/user/quarkism

using System;
using System.Linq;
using UnityEngine;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
[System.AttributeUsage (System.AttributeTargets.Method)]
public class EditorButtonAttribute : PropertyAttribute
{
}

#if UNITY_EDITOR
[CustomEditor (typeof (MonoBehaviour), true)]
public class EditorButton : Editor
{
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		var mono = target as MonoBehaviour;

		var methods = mono.GetType ()
			.GetMembers (BindingFlags.Instance | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
				BindingFlags.NonPublic)
			.Where (o => Attribute.IsDefined (o, typeof (EditorButtonAttribute)));

		foreach (var memberInfo in methods)
		{
			GUILayout.Space (10f);
			if (GUILayout.Button (memberInfo.Name.AddSpacesBeforeCaps ()))
			{
				var method = memberInfo as MethodInfo;
				method.Invoke (mono, null);
			}
			//			GUILayout.Space(10f);
		}
	}
}
#endif

public static class StringExtension
{
	/// <summary>
	/// Helper function to ensure a string is not null or empty
	/// </summary>
	public static bool IsValid (this string stringValue)
	{
		return !String.IsNullOrEmpty (stringValue);
	}

	public static string AddSpacesBeforeCaps (this string stringValue)
	{
		return System.Text.RegularExpressions.Regex.Replace (stringValue, "[A-Z]", " $0");
	}
}