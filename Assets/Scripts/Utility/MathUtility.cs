﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra
{
	public static class MathUtility
	{
		public static float NoimalizeInRange (float min, float max, float value)
		{
			return (value - min) / (max - min);
		}

	}
}
