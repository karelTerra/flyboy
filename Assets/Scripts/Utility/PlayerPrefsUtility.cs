﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terra
{
	public static class PlayerPrefsUtility
	{
		public static void ClearPlayerPref ()
		{
			PlayerPrefs.DeleteAll ();
		}

		public static void ClearPlayerPref (string key)
		{
			if (PlayerPrefs.HasKey (key))
				PlayerPrefs.DeleteKey (key);
		}

	}
}
