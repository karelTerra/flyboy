﻿Shader "Custom/Paralax3"
{
	Properties
	{
		_BackTex("Back Texture", 2D) = "white" {}
		_BackScroll("Back Scroll", Float) = 0

		_MidTex("Mid Texture", 2D) = "white" {}
		_MidScroll("Mid Scroll", Float) = 0

		_ForeTex("Fore Texture", 2D) = "white" {}
		_ForeScroll("Fore Scroll", Float) = 0
	}

	SubShader
	{
		Tags { "RenderType"="Transparent" }
		
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				float2 uv2 : TEXCOORD2;
				float4 vertex : SV_POSITION;
			};

			sampler2D _BackTex;
			sampler2D _MidTex;
			sampler2D _ForeTex;
			fixed4 _BackTex_ST;
			fixed4 _MidTex_ST;
			fixed4 _ForeTex_ST;

			fixed _BackScroll;
			fixed _MidScroll;
			fixed _ForeScroll;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _BackTex);
				o.uv1 = TRANSFORM_TEX(v.uv, _MidTex);
				o.uv2 = TRANSFORM_TEX(v.uv, _ForeTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float2 animatedUV = i.uv;
				float2 animatedUV1 = i.uv1;
				float2 animatedUV2 = i.uv2;
				animatedUV.x += _BackScroll;
				animatedUV1.x += _MidScroll;
				animatedUV2.x += _ForeScroll;

				fixed4 col;
				fixed4 backCol = tex2D(_BackTex, animatedUV);
				fixed4 midCol = tex2D(_MidTex, animatedUV1);
				fixed4 foreCol = tex2D(_ForeTex, animatedUV2);

				col = lerp(backCol, midCol, step(1, midCol.a));
				col = lerp(col, foreCol, step(1, foreCol.a));

				return col;
			}
			ENDCG
		}
	}
}
