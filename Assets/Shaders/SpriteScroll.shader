﻿Shader "Custom/Sprites/Scrolling"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_Scroll("Scroll", Float) = 0
		_Offset("Offset", Float) = 0
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma shader_feature ETC1_EXTERNAL_ALPHA

			#include "UnityCG.cginc"
			
			#define IF(a, b, c) lerp(b, c, step((fixed) (a), 0))

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color : COLOR;
				float2 uv  : TEXCOORD0;
			};

			fixed4 _Color;
			fixed _Scroll;
			fixed _Offset;

			sampler2D _MainTex;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.uv = IN.uv, _MainTex;
				OUT.color = IN.color * _Color;

#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap(OUT.vertex);
#endif	
				return OUT;
			}
	

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed2 animatedUV = IN.uv;
				animatedUV.x += _Scroll + _Offset;

				fixed4 c = tex2D(_MainTex, animatedUV) * IN.color;
				c.rgb *= c.a;
				return c;
			}
			
			ENDCG
		}
	}
}